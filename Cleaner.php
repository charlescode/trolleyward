<?php
namespace TrolleyWard;

class Cleaner extends Person 
{
	public function cleanWard()
	{
		return "clean ward is clean's job";	
	}

	public function putBinPanroom($bin, &$bins) 
	{
		array_push($bins['panroom'], $bin);
	}

	public function putBinDisposal($bin, &$bins)
	{
		array_push($bins['disposal'], $bin);
	}

}