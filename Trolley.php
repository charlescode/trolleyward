<?php
namespace TrolleyWard;

/**
 * in hospital normally use trolley to carry and move bin, the trolley can hold 6 bins one time
 * the trolley can hook 2 bins and carry 4 bins
 */	
class Trolley
{
	protected $wheel;
	protected $space;
	protected $maxSpace = 6;
	protected $color;
	//current hook amount
	protected $hookCount;
	//current carry amount
	protected $carryCount;
	protected $handle;
	protected $takenBins = [
		'hook' => [],
		'carry' => []
	];

	const MAX_HOOK_BINS = 2;
	const MAX_CARRY_BINS = 4;

	// these are type of bins which are used in hospital, space means how many space the bin 
	// can be occupied, maxTake means how many the bins can be taken in the trolley. 
	// all types of bin can be carried, but only three type of bins can be hooked.
	protected $carryBins = [
		'general' => [
			'space' => 1,
			'maxTake' => 4
		],
		'commingle' => [
			'space' => 1,
			'maxTake' => 4
		],
		'cardboard' => [
			'space' => 1,
			'maxTake' =>4	
		],
		'clinical' => [
			'space' => 0.5,
			'maxTake' => 8
		],
		'pharmacy' => [
			'space' => 0.5,
			'maxTake' => 8
		],
		'bodypart' => [
			'space' => 0.5,
			'maxTake' => 8
		],
		'cytotoxic' => [
			'space' => 0.5,
			'maxTake' => 8
		]

	];

	//hookBins means only these three types of bin can hook on the trolley.
	protected $hookBins = [
		'general' => 2,
		'commingle' => 2,
		'cardboard' => 2,
	];
	
	public function turn()
	{
		return "the trolley can turn the direction";
	}

	public function forward()
	{
		return "the trolley can move forwrd";
	}

	public function backward()
	{
		return "the trolley can move backward";
	}


	public function hook(string $bin) : bool
	{

		if (!array_key_exists($bin, $this->hookBins)){
			return false;
		} 
		if ($this->hookCount >= Trolley::MAX_HOOK_BINS) {
			return false;
		} 
		$this->hookCount++;
		//put the bin into the takenBins array.
		array_push($this->takenBins['hook'], $bin);
		return true;
	}

	public function carry(string $bin) : bool
	{
		$sum = 0;

		if (!array_key_exists($bin, $this->carryBins)) {
			return false;
		} 
		if ($this->carryCount >= Trolley::MAX_CARRY_BINS) {
			return false;
		} 
		array_push($this->takenBins['carry'], $bin);
		$this->carryCount += $this->carryBins[$bin]['space'];
		return true;
	}

	public function viewTakeBins()
	{
		$bins = '';
		$bins.= implode(',',$this->takenBins['hook']);
		if (strlen($bins)){
			$bins.=',';
		}
		$bins.= implode(',',$this->takenBins['carry']);
		return $bins;
	}

	//take bin from ward.
	public function trolleyTakeWard(Ward &$ward) {
		$bins = [];
		$wardBins = $ward->getWardBin();
		// var_dump($wardBins);
		foreach ($wardBins as $key => $value) {
			foreach ($value as $item => $val) {
				//in hookBin array, first hook bin then carry
				if (array_key_exists($val , $this->hookBins)) {
					if ($this->hookCount < Trolley::MAX_HOOK_BINS) {
						if ($this->hook($val)) {
							// unset($bins[$key]);
							$ward->removeBin($key, $val);
						}
					} else {
						if (array_key_exists($val, $this->carryBins)){
							if ($this->carry($val)) {
								// unset($bins[$key]);
								$ward->removeBin($key, $val);
							}
						}
					}

				} else if (array_key_exists($val, $this->carryBins)){
					if ($this->carry($val)) {
						// unset($bins[$key]);
						$ward->removeBin($key, $val);
					}  
				} else {
					//because $val is not valid bin, so just remove it.
					$ward->removeBin($key, $val);
				}

			}
		}
		echo "Trolley take bin from ward ".$ward->getWardName()." <br>";
		echo "Take bins list ".$this->viewTakeBins()."<br>";
		echo "Left bin need take next time<br>";
		var_dump($ward->getWardBin());
		$wardLeftBin = $ward->getWardBin();
		//after loop finish, 2 condition, first take all the bins, need tip trolley[trolley maybe has more space or full], 
		//second still left ward's bin, so also need to tip the full trolley.
		//if carry space > 3, means carry space = 3.5 [left half space, but next bin need 1 space, trolley is full]
		//carry space = 4 bin full.
		if ($this->carryCount > Trolley::MAX_CARRY_BINS - 1) {
			$this->tipTrolley();
		}
		//left bins need take next time
		//this is very important part, how to use recursive all in class
		if (count($wardLeftBin['panroom']) >0 || count($wardLeftBin['disposal']) >0) {
			return $this->trolleyTakeWard($ward);
		}
		return $this->takenBins;
	}

	//After take bins from ward, move trolley to the big disposal place, empty all the bins
	public function tipTrolley() 
	{
		$tipped = [];
		if ($this->hookCount > 0) {
			foreach ($this->takenBins['hook'] as $key => $value) {
				unset($this->takenBins['hook'][$key]);
				echo "tip current bin $value<br>";
				$this->hookCount = 0;
				$tipped[] = $value;
			}
		}
		
		if ($this->carryCount > 0) {
			foreach ($this->takenBins['carry'] as $key => $value) {
				unset($this->takenBins['carry'][$key]);
				echo "tip current bin $value<br>";
				$this->carryCount = 0;
				$tipped[] = $value;
			}
		}
		return $tipped;
	}

}