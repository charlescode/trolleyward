<?php
use TrolleyWard\Nurse;
use TrolleyWard\Cleaner;
use TrolleyWard\Ward;
use TrolleyWard\Trolley;

require_once('/var/www/html/practise/hospital/trolleyward/Person.php');
require_once('/var/www/html/practise/hospital/trolleyward/Nurse.php');
require_once('/var/www/html/practise/hospital/trolleyward/Cleaner.php');
require_once('/var/www/html/practise/hospital/trolleyward/Trolley.php');
require_once('/var/www/html/practise/hospital/trolleyward/Ward.php');


$nurse = new Nurse('Mary', '50');
$binArr = [
	'panroom' =>[],
	'disposal' =>[]
];
$nurse->putBinPanroom('general', $binArr);
$nurse->putBinDisposal('cardboard', $binArr);

$cleaner = new Cleaner('Ta Jun', '55');
$cleaner->putBinDisposal('commingle', $binArr);
$cleaner->putBinPanroom('clinical', $binArr);

$jira = new Cleaner('Jia Ria', '47');
$jira->putBinDisposal('bodypart', $binArr);
$jira->putBinDisposal('general', $binArr);

$jira->putBinPanroom('cytotoxic', $binArr);
$jira->putBinPanroom('cardboard', $binArr);

$michelle = new cleaner('Michelle', 46);



// var_dump($binArr);

$ward = new Ward('ED');
$ward->addNurse($nurse);
$ward->addCleaner($cleaner);

$ward->setDisposal($nurse, 'commingle');
$ward->setPanroom($cleaner, 'general');

$ward->addCleaner($jira);
$ward->setDisposal($jira, 'bodypart');
$ward->setDisposal($jira, 'general');

$ward->setPanroom($jira, 'cytotoxic');
$ward->setPanroom($jira, 'cardboard');

$ward->setDisposal($jira, 'commingle');
$ward->setDisposal($jira, 'general');

$ward->setPanroom($jira, 'cytotoxic');
$ward->setPanroom($jira, 'clinical');

$ward->setPanroom($jira, 'cytotoxic');
$ward->setPanroom($jira, 'pharmacy');

$bins = $ward->viewBins();
echo "bins first is $bins<br>";


$trolley = new Trolley();
$trolley->trolleyTakeWard($ward);

//Important, when only trolley take bin from one ward, then in the end call this function tipTolley() 
//$trolley->tipTrolley();

$wardICU = new Ward('ICU');
$wardICU->addCleaner($michelle);

$wardICU->setPanroom($michelle, 'cytotoxic');
$wardICU->setPanroom($michelle, 'cardboard');

$wardICU->setDisposal($michelle, 'bodypart');
$wardICU->setDisposal($michelle, 'general');


$wardICU->setDisposal($michelle, 'commingle');
$wardICU->setDisposal($michelle, 'cardboard');

$wardICU->setPanroom($michelle, 'pharmacy');
$wardICU->setPanroom($michelle, 'clinical');

$wardICU->setPanroom($michelle, 'cytotoxic');
$wardICU->setPanroom($michelle, 'bodypart');
//trolley continue to take ICU ward, if trolley is no full, continue his journey.
$trolley->trolleyTakeWard($wardICU);
//in the end, tip trolley
$trolley->tipTrolley();




