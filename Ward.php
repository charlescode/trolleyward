<?php
namespace TrolleyWard;
use TrolleyWard\Nurse;
use TrolleyWard\Cleaner;

/**
 * ward is the place where patients live and get service place, nurses and doctors service patient
 * cleaners clean the ward
 */  	 
class Ward {
	protected $name;
	protected $wardBin = [
		'panroom' => [],
		'disposal' =>[]
	];
	protected $cleaner = [];
	protected $nurse = [];
	protected $doctor = [];

	function __construct(string $name) 
	{
		$this->name = $name;
	}

	function getWardName() 
	{
		return $this->name;
	}

	public function addNurse(Nurse $nurse) 
	{
		$this->nurse[$nurse->getName()] = $nurse;
	}	

	public function addCleaner(Cleaner $cleaner) 
	{
		$this->cleaner[$cleaner->getName()] = $cleaner;
	}

	public function setPanroom(Person $person, string $bin)
	{
		if ($person instanceOf Nurse){
			if (array_key_exists($person->getName(), $this->nurse)) {

				$person->putBInPanroom($bin, $this->wardBin);
			}
		} else if ($person instanceOf Cleaner) {
			if (array_key_exists($person->getName(), $this->cleaner)) {

				$person->putBInPanroom($bin, $this->wardBin);
			}
		}
	}

	public function setDisposal(Person $person, string $bin)
	{
		if ($person instanceOf Nurse){
			if (array_key_exists($person->getName(), $this->nurse)) {

				$person->putBInDisposal($bin, $this->wardBin);
			}
		} else if ($person instanceOf Cleaner) {
			if (array_key_exists($person->getName(), $this->cleaner)) {

				$person->putBInDisposal($bin, $this->wardBin);
			}
		}
	}


	public function viewBins()
	{
		$bins = '';
		echo 'panroom Bins<br>';	
		// var_dump($this->panroomBin);
		// var_dump($this->disposalBin);
		$bins .= implode(',', $this->wardBin['panroom']);
		echo '<br>disposal Bins<br>';
		$bins .= implode(',', $this->wardBin['disposal']);
		
		return $bins;
	}

	public function getWardBin() 
	{
		return $this->wardBin;
	}

	public function removeBin(string $key, string $value) : bool 
	{
		if ($key == 'panroom' || $key == 'disposal') {
			foreach ($this->wardBin[$key] as $item => $val) {
				if ($value == $val) {
					unset($this->wardBin[$key][$item]);
					return true;
				}
			}
		}
		return false;
	}
}