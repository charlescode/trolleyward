<?php
namespace TrolleyWard;


class Nurse extends Person 
{
	public function lookatPatient()
	{
		return "nurse looks after patient";
	}

	public function putBinPanroom($bin, &$bins)	 
	{
		if (array_key_exists('panroom', $bins)) {
			array_push($bins['panroom'], $bin);
		}
	}

	public function putBinDisposal($bin, &$bins)
	{
		if (array_key_exists('disposal', $bins)) {
			array_push($bins['disposal'], $bin);
		}
	}

}

