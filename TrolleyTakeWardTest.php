<?php

use PHPUnit\Framework\TestCase;
use TrolleyWard\Person;
use TrolleyWard\Cleaner;
use TrolleyWard\Nurse;
use TrolleyWard\Ward;
use TrolleyWard\Trolley;

require_once('/var/www/html/practise/hospital/trolleyward/Person.php');
require_once('/var/www/html/practise/hospital/trolleyward/Cleaner.php');
require_once('/var/www/html/practise/hospital/trolleyward/Nurse.php');
require_once('/var/www/html/practise/hospital/trolleyward/Ward.php');
require_once('/var/www/html/practise/hospital/trolleyward/Trolley.php');



class TrolleyTakeWard extends TestCase
{

    protected function setUp() 
    {


    }

    public function testWard() 
    {
    	$nurse = new Nurse('Julie', '40');
    	$cleaner = new Cleaner('Michelle', '46');
    	$ward = new Ward('ED');
    	$ward->addNurse($nurse);
    	$ward->addCleaner($cleaner);
		$ward->setPanroom($nurse, 'commingle');
		$ward->setDisposal($nurse, 'cardboard');

		$ward->setPanroom($cleaner, 'general');
		$ward->setDisposal($cleaner, 'clinical');

		$bins = $ward->getWardBin();

		$this->assertArrayHasKey('panroom' , $bins);
		$this->assertArrayHasKey('disposal', $bins);
		$this->assertContains('commingle', $bins['panroom']);
		$this->assertContains('general', $bins['panroom']);
		$this->assertContains('cardboard', $bins['disposal']);
		$this->assertContains('clinical', $bins['disposal']);

		$this->assertTrue($ward->removeBin('disposal', 'cardboard'));
		$this->assertFalse($ward->removeBin('disposal','abc'));

    }
 	
 	public function testTrolleyFunctions() 
 	{
 		$trolley = new Trolley();
 		$bin = 'commingle';
 		$this->assertTrue($trolley->carry($bin));
 		$this->assertTrue($trolley->hook($bin));

 		$smallBin = 'bodypart';
		$this->assertTrue($trolley->carry($smallBin));
 		$this->assertFalse($trolley->hook($smallBin));

 		$this->assertContains('bodypart', $trolley->viewTakeBins());
 		$this->assertContains('commingle', $trolley->viewTakeBins());

 		$tipBins = $trolley->tipTrolley();
 		var_dump($tipBins);
 		$this->assertContains('bodypart', $tipBins);
 		$this->assertContains('commingle', $tipBins);
 		$this->assertEquals(3, count($tipBins));



 	}

 	public function testTrolleyTakeWard() 
 	{
 		//test case 1 no recursive.
		$nurse = new Nurse('Julie', '40');
    	$cleaner = new Cleaner('Michelle', '46');
    	$ward = new Ward('ED');
    	$ward->addNurse($nurse);
    	$ward->addCleaner($cleaner);
		$ward->setPanroom($nurse, 'commingle');
		$ward->setDisposal($nurse, 'cardboard');
		$ward->setDisposal($nurse, 'bodypart');

		$ward->setPanroom($cleaner, 'general');
		$ward->setDisposal($cleaner, 'clinical');
		$ward->setDisposal($cleaner, 'cytotoxic');
		$ward->setDisposal($cleaner, 'toy');




		$trolley = new Trolley();
		$takeBins = $trolley->trolleyTakeWard($ward);
		$this->assertContains('cytotoxic', $takeBins['carry']);

		$this->assertEquals(4, count($takeBins['carry']));
		$this->assertEquals(2, count($takeBins['hook']));
		$this->assertFalse(in_array('toy', $takeBins['carry']));
		$this->assertFalse(in_array('toy', $takeBins['hook']));
		$this->assertFalse(in_array('cytotoxic', $takeBins['hook']));

		//test case 2 recursive call

		$nurse = new Nurse('Julie', '40');
    	$cleaner = new Cleaner('Michelle', '46');
    	$ward = new Ward('ED');
    	$ward->addNurse($nurse);
    	$ward->addCleaner($cleaner);
		$ward->setPanroom($nurse, 'commingle');
		$ward->setDisposal($nurse, 'cardboard');
		$ward->setDisposal($nurse, 'bodypart');
		$ward->setPanroom($nurse, 'pharmacy');
		$ward->setDisposal($nurse, 'clinical');
		$ward->setDisposal($nurse, 'bodypart');

		$ward->setDisposal($cleaner, 'clinical');
		$ward->setPanroom($cleaner, 'general');

		$ward->setDisposal($cleaner, 'cytotoxic');
		$ward->setDisposal($cleaner, 'clinical');
		$ward->setDisposal($cleaner, 'cardboard');
		$ward->setDisposal($cleaner, 'pharmacy');
		$ward->setDisposal($cleaner, 'bodypart');
		$ward->setDisposal($cleaner, 'toy');




		$trolley = new Trolley();
		$takeBins = $trolley->trolleyTakeWard($ward);
		$this->assertFalse(in_array('cytotoxic', $takeBins['carry']));

		$this->assertEquals(3, count($takeBins['carry']));
		$this->assertEquals(1, count($takeBins['hook']));
		$this->assertFalse(in_array('toy', $takeBins['carry']));
		$this->assertFalse(in_array('toy', $takeBins['hook']));
		$this->assertTrue(in_array('clinical', $takeBins['carry']));
		$this->assertTrue(in_array('cardboard', $takeBins['hook']));
		$this->assertTrue(in_array('bodypart', $takeBins['carry']));

		$trolley->tipTrolley();


 	}

    protected function tearDown() {


    }
}