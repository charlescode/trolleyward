<?php
namespace TrolleyWard;

class Person 
{
	protected $name;
	protected $age;

	function __construct(string $name, string $age) {
		$this->name = $name;
		$this->age = $age;

	}

	public function getName() : string {
		return $this->name;
	}

}